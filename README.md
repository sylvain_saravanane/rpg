# rpg_sylvain

-- Logique du jeu simple que je veux démontrer

Demander le pseudo et le choix de team au joueur. 

Lancer une boucle pour chaque monstre : 

Le joueur peut choisir une potion d'attaque. 

Affronter le monstre avec la potion d'attaque adéquate

Après avoir battu les trois monstres, le jeu est terminé.



SCENARIO DU RPG

“Bienvenue sur RPG” 

“Entrez votre pseudo : -------------” 

“Choisis ta team : EAU – FEU – FOUDRE” ( à noter que le choix de l’équipe n’a pas d’impact sur la potion) 

Paramétrage :  

pseudo, team, point de vie : 200 points de vie, épée : 20 dégats, potion de vie : 100 points de vie (utilisable si le point de vie du joueur est inférieur)   

 

Début de l’aventure :  


L’aventurier a 3 chemins, il en choisit un et tombe sur un monstre, il choisit la potion adapté puis le combat. Après l’avoir vaincu, il a le choix entre 2 chemin, puis le dernier chemin avant d’affronter le boss Darkos 

 

Les 3 monstres : 

 

Monstre 1 (Aquadox) : Type EAU dans le chemin 1 

Monstre 2 (Firelost) : Type FEU dans le chemin 2 

Monstre 3 (Voltek) : Type FOUDRE dans le chemin 3 

 

Avant l’affrontement de chaque monstre, le joueur doit choisir une potion d’attaque chez le marchand pour améliorer son épée. Elle est utilisée une fois, lorsqu’un monstre est battu. 

 

Marchand propose : 

POTION EAU (100 dégâts en plus pour l’épée) 

POTION FEU (100 dégâts en plus pour l’épée) 

POTION FOUDRE (100 dégâts en plus pour l’épée) 

 

POTION EAU > POTION FEU ;  

POTION FEU > POTION FOUDRE ;  

POTION FOUDRE > POTION EAU ;  

 

POTION EAU (+300% de dégats sur Firelost ET –50% de dégats sur Voltek) 

POTION FEU (+300% de dégats sur Voltek ET –50% de dégats sur Aquadox) 

POTION FOUDRE (+300% de dégats sur Aquadox ET –50% de dégats sur Firelost) 

 

Les PV et attaques des Monstres : voir sur le code

 

MONSTRE 

PV 

POINTS D’ATTAQUE 

AQUADOX 

500 

10 

FIRELOST 

500 

10 

VOLTEK 

500 

10 

 

 

----------------------------------------------------------- 

 

(après le choix du chemin et avoir pris connaissance du monstre, avant le combat, le marchand lui propose un des 3 potions (EAU, FEU, FOUDRE), le guerrier choisit la potion la plus adapté pour le combat)  

 

POUR UTILISER LA POTION DE VIE, saisir “vie” seulement si le joueur à moins de 100 points de vie, ou après le combat en citant “Voulez-vous utiliser votre potion ? Saisir O pour oui ou N pour non” 

 

Scénario de combat : 

 

Le monstre attaque : {monstre} utilise {AttaqueType} 

Le guerrier attaque : JoueurAttaque en fonction de la potion utilisé 

 

Jusqu’a que l’un deux meurt 

 


utilisation des classes pour les personnages, les monstres, le marchand et potion pour le déroulement du jeu. Voici un exemple de structure pour les entités impliquées : 

Les entités : 

Joueur : 

Pseudo 

Team (EAU, FEU, FOUDRE) 

Points de vie (200) 

Épée (20 dégâts) 

Potion de vie (100 points de vie, utilisable si les PV du joueur sont inférieurs à 100) 

Marchand : 

Propose des potions d'attaque pour améliorer l'épée 

Monstres : 

Aquadox (EAU, 500 PV, 10 points d'attaque) 

Firelost (FEU, 500 PV, 10 points d'attaque) 

Voltek (FOUDRE, 500 PV, 10 points d'attaque)
